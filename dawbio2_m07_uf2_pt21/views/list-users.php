<table>
    <h2>List all users</h2>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Role</th>
        <th>e-mail</th>
    </tr>
    <?php
    //display list of users in a table.
    $userList = $params['userList'];
    // $params contains variables passed in from the controller.
    foreach ($userList as $user) {
        echo <<<EOT
        <tr>
            <td>{$user->getId()}</td>
            <td><a href="index.php?id={$user->getId()}&action=findUser">{$user->getUsername()}</a></td>
            <td>{$user->getRole()}</td>
            <td>{$user->getemail()}</td>
        </tr>               
EOT;
    }
    ?>
</table>
