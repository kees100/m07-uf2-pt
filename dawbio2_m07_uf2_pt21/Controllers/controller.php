<?php
require_once './lib/ViewLoaded.php';
require_once './Model/model.php';
require_once './lib/ProductFormValidation.php';
require_once './lib/UserFormValidation.php';

/**
 * Main controller of Proven Store application.
 * @author ProvenSoft
 */
class Controller {
    /**
     * @var ViewLoader
     */
    private $myView;
    /**
     * @var Model 
     */
    private $myModel;
    /**
     * @var string  
     */
    private $action;
    
    public function __construct() {
        //instantiate the view loader.
        $this->myView = new ViewLoader();
        //instantiate the model.
        $this->myModel = new Model();
    }
    
    /**
     * Process requests from the client, regarding action command.
     */
    public function processRequest() {
        $this->action = "";
        //retrieve action command requested by the client.
        $requestMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        switch ($requestMethod) {
            case 'get':
            case 'GET':
                $this->processGet();
                break;
            case 'post':
            case 'POST':
                $this->processPost();
                break;
            default:
                break;
        }
    }
    
    /**
     * Process GET requests from the client, regarding action command.
     */
    private function processGet() {
        $this->action = "";
        //retrieve action command requested by the client.
        if (filter_has_var(INPUT_GET, 'action')) {
            $this->action = filter_input(INPUT_GET, 'action'); 
        }
    
        //process action.
        switch ($this->action) {
            case 'home':
                $this->homePage();
                break;
            case 'login':
                $this->showLoginFrom();
                break;
            case 'logout':
                $this->displayLogout();
                break;
            case 'users/listAll':
                $this->listAllUsers();
                break;
            case 'products/listAll':
                $this->listAllProducts();
                break;
            case 'userForm':
                $this->UserForm();
                break;
            case 'findUser':
                $this->findUser();
                break;
            case 'addUser':
                $this->addNewUser();
                break;
            case 'updateUser':
                $this->modifyUser();
                break;
            case 'deleteUser':
                $this->removeUser();
                break;
            case 'productForm':
                $this->formProducts();
                break;
            case 'findProduct':
                $this->findProduct();
                break;
            case 'addProduct':
                $this->addNewProduct();
                break;
            case 'updateProduct':
                $this->modifyProduct();
                break;
            case 'removeProduct':
                $this->removeProduct();
                break;
            default:
                $this->homePage();
                break;
        }
    }
     
    /**
     * Process POST requests from the client, regarding action command.
     */
    private function processPost() {
        $this->action = "";
        if (filter_has_var(INPUT_POST, 'action')) {
            $this->action = filter_input(INPUT_POST, 'action');
        }
     
        switch ($this->action) {
            case 'user/login':
                $this->doUserLogin();
                break;
            case 'user/logout':
                $this->logout();
                break;
            default:
                break;
        }
    }
    
    /**
     * Displays home page content.
     */
    public function homePage() {
        $this->myView->show("home.php", null);
    }

    /**
     * Displays login page content.
     */
    public function showLoginFrom() {
        $this->myView->show("login.php", null);
    }

    /**
     * Processes user login.
     */
    private function doUserLogin() {
        // Check if the login form was submitted
        if (isset($_POST['user/login'])) {
            // Get and sanitize the input data from the form
            $username = htmlspecialchars(trim($_POST['username']));
            $password = htmlspecialchars(trim($_POST['password']));

            // Check if username and password are not empty
            if (empty($username) || empty($password)) {
                $data['result'] = "Please fill in the data.";
                $this->myView->show("login.php", $data);
            }
            
            // Ask the model to check for a user with given credentials
            $checkedUser = $this->myModel->searchUserByCredentials($username, $password);
            
            if ($checkedUser) {
                // Save data to session
                
                $_SESSION['userValid'] = $checkedUser;
                $_SESSION['username'] = $checkedUser->getUsername();
                $_SESSION['password'] = $checkedUser->getPassword();
                $_SESSION['role'] = $checkedUser->getRole();
        
                // Forward to view
                header("Location: $_SERVER[PHP_SELF]");
                exit();
                // $this->myView->show("home.php", null);
            } else {
                $data['result'] = "Invalid credentials";
                $data['username'] = $username;
                $this->myView->show("login.php", $data);
            }
        }
    }

    /**
     * Displays logout page content.
     */
    public function displayLogout() {
        $this->myView->show("logout.php", null);
    }

    /**
     * Logs the user out.
     */
    public function logout() {
        session_unset();
        session_destroy();
        header("Location: $_SERVER[PHP_SELF]");
        exit();
        // $this->myView->show("home.php", null);
    }

    /**
     * Lists all users control method.
     */
    public function listAllUsers() {
        // Get all users.
        $userList = $this->myModel->searchAllUsers();
        // Send data to the template.
        $data['userList'] = $userList;
        // Show the template with the given data.
        $this->myView->show("list-users.php", $data);
    }    

    /**
     * Lists all products control method.
     */
    public function listAllProducts() {
        // Get all products.
        $productList = $this->myModel->searchAllProducts();
        // Send data to the template.
        $data['productList'] = $productList;
        // Show the template with the given data.
        $this->myView->show("list-products.php", $data);
    }  

    /**
     * Displays a form for a product.
     */
    public function formProducts() {
        $product = ProductFormValidation::getData();
        // Pass data to the template.
        $data['product'] = $product;
        $data['action'] = $this->action;
        $this->myView->show("product-form.php", $data);
    }

    /**
     * Shows a form for a user.
     */
    public function UserForm() {
        $user = UserFormValidation::getData();
        // Pass data to the template.
        $data['user'] = $user;
        $data['action'] = $this->action;
        $this->myView->show("user-form.php", $data);
    }

    /**
     * Finds a user by id from action and if the user exists, show user form data.
     * If the user does not exist, show an error message.
     * TODO
     */
    public function findUser() {
        $user = UserFormValidation::getData();
        $result = null;

        if (is_null($user)) {
            $result = "Error reading user";
        } else {
            $userFound = $this->myModel->searchUserById($user->getId());
        
            if (!is_null($userFound)) {
                // Pass data to the template.
                $data['user'] = $userFound;
                $data['action'] = "change";
            } else {
                $result = "User not found";
            }            
        }
        // Pass data to the template.
        $data['result'] = $result;
        // Show the template with the given data.
        $this->myView->show("user-form.php", $data);   
    }

    /**
     * Requests the model to add a user sent by the form.
     */
    public function addNewUser() {
        $user = UserFormValidation::getData();
        $result = null;

        if (is_null($user)) {
            $result = "Error adding user";
        } else {
            $numAffected = $this->myModel->addUser($user);
            if ($numAffected > 0) {
                $result = "User successfully added";
            } else {
                $result = "Error adding user";
            }            
        }
        // Pass data to the template.
        $data['result'] = $result;
        $data['user'] = $user;
        // Show the template with the given data.
        $this->myView->show("user-form.php", $data);
    }

    /**
     * Requests the model to modify a user sent by the form.
     */
    public function modifyUser() {
        $user = UserFormValidation::getData();
        $result = null;

        if (is_null($user)) {
            $result = "Error reading user";
        } else {
            $numAffected = $this->myModel->updateUser($user);
            if ($numAffected > 0) {
                $result = "User successfully modified";
            } else {
                $result = "Error modifying user";
            }            
        }
        // Pass data to the template.
        $data['result'] = $result;
        $data['user'] = $user;
        // Show the template with the given data.
        $this->myView->show("user-form.php", $data);        
    }

    /**
     * Requests the model to remove an item sent by the form.
     */
    public function removeUser() {
        $user = UserFormValidation::getData();
        $result = null;

        if (is_null($user)) {
            $result = "Error reading user";
        } else {
            $numAffected = $this->myModel->deleteUser($user);
            if ($numAffected > 0) {
                $result = "User successfully removed";
            } else {
                $result = "Error removing user";
            }
        }
        // Pass data to the template.
        $data['result'] = $result;
        // Show the template with the given data.
        $this->myView->show("user-form.php", $data);          
    }

    /**
     * Finds a product by id from action and if the product exists, show product form data.
     * If the product does not exist, show an error message.
     * TODO
     */
    public function findProduct() {
        $product = ProductFormValidation::getData();
        $result = null;

        if (is_null($product)) {
            $result = "Error reading product";
        } else {
            $productFound = $this->myModel->searchProductById($product->getId());
        
            if (!is_null($productFound)) {
                // Pass data to the template.
                $data['product'] = $productFound;
                $data['action'] = "change";
            } else {
                $result = "Product not found";
            }            
        }
        // Pass data to the template.
        $data['result'] = $result;
        // Show the template with the given data.
        $this->myView->show("product-form.php", $data);   
    }

    /**
     * Requests the model to add a product sent by the form.
     */
    public function addNewProduct() {
        $product = ProductFormValidation::getData();
        $result = null;

        if (is_null($product)) {
            $result = "Error adding product";
        } else {
            $numAffected = $this->myModel->addProduct($product);
            if ($numAffected > 0) {
                $result = "Product successfully added";
            } else {
                $result = "Error adding product";
            }            
        }
        // Pass data to the template.
        $data['result'] = $result;
        $data['product'] = $product;
        // Show the template with the given data.
        $this->myView->show("product-form.php", $data);
    }

    /**
     * Requests the model to modify a product sent by the form.
     */
    public function modifyProduct() {
        $product = ProductFormValidation::getData();
        $result = null;

        if (is_null($product)) {
            $result = "Error reading product";
        } else {
            $numAffected = $this->myModel->updateProduct($product);
            if ($numAffected > 0) {
                $result = "Product successfully modified";
            } else {
                $result = "Error modifying product";
            }            
        }
        // Pass data to the template.
        $data['result'] = $result;
        $data['product'] = $product;
        // Show the template with the given data.
        $this->myView->show("product-form.php", $data);        
    }


/**
 * This function is responsible for handling the removal of a product.
 */
public function removeProduct() {
    // Retrieve product data from the form using a validation method.
    $product = ProductFormValidation::getData();
    
    // Initialize the result variable to null.
    $result = null;
    
    // Check if product data is successfully retrieved.
    if (is_null($product)) {
        // If there is an issue reading the product, set an error message.
        $result = "Error reading product";
    } else {
        // Attempt to delete the product using the model's deleteProduct method.
        $numAffected = $this->myModel->deleteProduct($product);
        
        // Check if the deletion was successful.
        if ($numAffected > 0) {
            // If the product is successfully removed, set a success message.
            $result = "Product successfully removed";
        } else {
            // If there is an issue removing the product, set an error message.
            $result = "Error removing product";
        }
    }
    
    // Prepare data to be passed to the template.
    $data['result'] = $result;
    
    // Show the template "product-form.php" with the given data.
    $this->myView->show("product-form.php", $data);
}

}