<?php
require_once 'model/user.php';

/**
 * Description of ItemFormValidation
 * Provides validation to get data from item form.
 * @author ProvenSoft
 */
class UserFormValidation {
    
    /**
     * validates and gets data from item form.
     * @return User the item with the given data or null if data is not present and valid.
     */
    public static function getData() {
        $userObj = null;
        $id = 0;
        //retrieve id sent by client.
        if (filter_has_var(INPUT_GET, 'id')) {
            $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT); 
        }
        $username = "";
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'username')) {
            $username = filter_input(INPUT_GET, 'username'); 
        }
        $password = "";
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'password')) {
            $password = filter_input(INPUT_GET, 'password'); 
        }
        $role = "";
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'role')) {
            $role = filter_input(INPUT_GET, 'role'); 
        }
        $email = "";
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'email')) {
            $email = filter_input(INPUT_GET, 'email'); 
        }
        // if (!empty($id) && !empty($username) && !empty($password) && !empty($role) && !empty($email)) { 
           // They exist and they are not empty.
        $userObj = new User($id, $username, $password, $role, $email);
        // }
        return $userObj;
    }
    
}
