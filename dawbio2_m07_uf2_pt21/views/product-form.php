<?php

if (isset($_SESSION['username']) && isset($_SESSION['role'])) {
   
    $username = $_SESSION['username'];
    $sessionRole  = $_SESSION['role'];
  
} else {
 
    $sessionRole = 'staff';
}
?>
<script type="text/javascript">
function submitForm(event) {
    var target = event.target;
    var buttonId = target.id;
    var myForm = document.getElementById('product-form');
    myForm.action.value = buttonId;
    myForm.submit();
    return false;
}
</script>
<?php

   $product = $params['product']??null;  //?? is the 'null coalescing operator'.
   $action = $params['action']??"findProduct";
   $result = $params['result']??null;
   if (is_null($product)) {
       $product = new Product(0, "", "", 0,0);
   }
   $disable = (($action == "findProduct")||($action == "productForm"))?"disabled":"";
   if (!is_null($result)) {
      echo <<<EOT
      <div><p class="alert">$result</p></div>
EOT;
   }

   if($sessionRole == 'admin') {
    echo <<<EOT
    <form id="product-form" method="get" action="index.php">
     <fieldset>
         <label for="id">Id: </label><input type="text" name="id" id="id" placeholder="enter id" value="{$product->getId()}"/>
         <label for="description">Description: </label><input type="text" name="description" id="description" placeholder="enter description" value="{$product->getDescription()}"/>
         <label for="size">Size: </label><input type="text" name="size" id="size" placeholder="enter size" value="{$product->getSize()}"/>
         <label for="price">Price: </label><input type="text" name="price" id="price" placeholder="enter price" value="{$product->getPrice()}"/>
         <label for="stock">Stock: </label><input type="text" name="stock" id="stock" placeholder="enter stock" value="{$product->getStock()}"/>
    </fieldset>
     <fieldset>
         <button type="button" id="findProduct" name="findProduct" onclick="submitForm(event);return false;">Find</button>
         <button type="button" id="addProduct" name="addProduct" onclick="submitForm(event);return false;">Add</button>
         <button type="button" id="updateProduct" name="updateProduct" onclick="submitForm(event);return false;">Modify</button>
         <button type="button" id="removeProduct" name="removeProduct" onclick="submitForm(event);return false;">Remove</button>
         <input name="action" id="action" hidden="hidden" value="add"/>
     </fieldset>
    </form>
    EOT;

   } 
   if($sessionRole === 'staff'){
      echo <<<EOT

      <form id="product-form" method="get" action="index.php">
       <fieldset>
       <label for="id">Id: </label><input type="text" name="id" id="id" placeholder="enter id" value="{$product->getId()}"/>
       <label for="description">Description: </label><input type="text" name="description" id="description" placeholder="enter description" value="{$product->getDescription()}"/>
       <label for="size">Size: </label><input type="text" name="size" id="size" placeholder="enter size" value="{$product->getSize()}"/>
       <label for="price">Price: </label><input type="text" name="price" id="price" placeholder="enter price" value="{$product->getPrice()}"/>
       <label for="stock">Stock: </label><input type="text" name="stock" id="stock" placeholder="enter stock" value="{$product->getStock()}"/>

      </fieldset>
        <fieldset>
           <button type="button" id="findProduct" name="findProduct" onclick="submitForm(event);return false;">Find</button>
           <button type="button" id="addProduct" name="addProduct" disabled onclick="submitForm(event);return false;">Add</button>
           <button type="button" id="updateProduct" name="updateProduct" disabled onclick="submitForm(event);return false;">Modify</button>
           <button type="button" id="removeProduct" name="removeProduct" disabled onclick="submitForm(event);return false;">Remove</button>
           <input name="action" id="action" hidden="hidden" value="add"/>
        </fieldset>
        </form>
    EOT;
   

   }
