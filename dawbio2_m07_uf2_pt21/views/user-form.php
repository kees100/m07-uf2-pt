<?php

if (isset($_SESSION['username']) && isset($_SESSION['role'])) {
   

    $username = $_SESSION['username'];
    $sessionRole  = $_SESSION['role'];
  
}
?>

<script type="text/javascript">
function submitForm(event) {
    var target = event.target;
    var buttonId = target.id;
    var myForm = document.getElementById('user-form');
    myForm.action.value = buttonId;
    myForm.submit();
    return false;
}
</script>

<?php

   $user = $params['user']??null;  //?? is the 'null coalescing operator'.
   $action = $params['action']??"findUser";
   $result = $params['result']??null;
   if (is_null($user)) {
       $user = new User(0, "");
   }
   $disable = (($action == "findUser")||($action == "userForm"))?"disabled":"";
   if (!is_null($result)) {
      echo <<<EOT
      <div><p class="alert">$result</p></div>
EOT;
   }

   if($sessionRole == 'admin') {
      echo <<<EOT
      <form id="user-form" method="get" action="index.php">
         <fieldset>
            <label for="id">Id: </label><input type="text" name="id" id="id" placeholder="enter id" value="{$user->getId()}"/><br>
            <label for="title">Username: </label><input type="text" name="username" id="username" placeholder="enter username" value="{$user->getUsername()}"/><br>
            <label for="password">Password: </label><input type="text" name="password" id="password" placeholder="enter password" value="{$user->getPassword()}"/><br>
            <label for="role">Role: </label><input type="text" name="role" id="role" placeholder="enter role" value="{$user->getRole()}"/><br>
            <label for="email">Email: </label><input type="text" name="email" id="email" placeholder="enter email" value="{$user->getEmail()}"/><br>
         </fieldset>
         <fieldset>
            <button type="submit" id="findUser" name="findUser" onclick="submitForm(event);return false;">Find</button>
            <button type="submit" id="addUser" name="addUser" onclick="submitForm(event);return false;">Add</button>
            <button type="submit" id="updateUser" name="updateUser" onclick="submitForm(event);return false;">Modify</button>
            <button type="submit" id="deleteUser" name="deleteUser" onclick="submitForm(event);return false;">Remove</button>
            <input name="action" id="action" hidden="hidden" value="add"/>
         </fieldset>
      </form>
      EOT;

   } 
   if($sessionRole === 'staff'){
      echo <<<EOT
      <form id="user-form" method="get" action="index.php">
         <fieldset>
            <label for="id">Id: </label><input type="text" name="id" id="id" placeholder="enter id" value="{$user->getId()}"/><br>
            <label for="title">Username: </label><input type="text" name="username" id="username" placeholder="enter username" value="{$user->getUsername()}"/><br>
            <label for="password">Password: </label><input type="text" name="password" id="password" placeholder="enter password" value="{$user->getPassword()}"/><br>
            <label for="role">Role: </label><input type="text" name="role" id="role" placeholder="enter role" value="{$user->getRole()}"/><br>
            <label for="email">Email: </label><input type="text" name="email" id="email" placeholder="enter email" value="{$user->getEmail()}"/><br>
         </fieldset>
         <fieldset>
            <button type="submit" id="findUser" name="findUser" onclick="submitForm(event);return false;">Find</button>
            <button type="submit" id="addUser" name="addUser" onclick="submitForm(event);return false;" disabled>Add</button>
            <button type="submit" id="updateUser" name="updateUser" onclick="submitForm(event);return false;" disabled>Modify</button>
            <button type="submit" id="deleteUser" name="deleteUser" onclick="submitForm(event);return false;" disabled>Remove</button>
            <input name="action" id="action" hidden="hidden" value="add"/>
         </fieldset>
      </form>
      EOT;
   

   }
   
  

   