<?php
include_once 'Model/model.php';

// Instantiate model
$myModel = new Model();

print_r ("*****************USERS*****************".PHP_EOL);
echo "<br>";
print_r("TEST: search all [find]".PHP_EOL);
echo "<br>";
$users = $myModel->searchAllUsers();
foreach($users as $user){
    echo $user;
    echo "<br>";
}

print_r("TEST: getUserByID [exist]".PHP_EOL);
echo "<br>";
$user = $myModel->searchUserById(1);
echo $user;
echo "<br>";

print_r("TEST: getUserByID [not exist]".PHP_EOL);
echo "<br>";
$user = $myModel->searchUserById(24);
echo "The user doesn't exist.";
echo "<br>";


print_r(PHP_EOL);
echo "<br>";
print_r("TEST: delete [null]".PHP_EOL);
$result = $myModel->deleteUser(new User());
print_r($result);
print_r(PHP_EOL);
echo "<br>";
print_r("TEST: delete [not exist]".PHP_EOL);
$result = $myModel->deleteUser(new User(24));
print_r($result);
print_r(PHP_EOL);
echo "<br>";
print_r("TEST: delete [exist]".PHP_EOL);
$result = $myModel->deleteUser(new User(3));
print_r($result);
print_r(PHP_EOL);
echo "<br>";


print_r("TEST: addUser [id not exist]");
$result = $myModel->addUser(new User(3, 'olga155', 'olga155', 'staff', 'olga155@gmail.com'));
print_r($result);
print_r(PHP_EOL);
echo "<br>";

print_r("TEST: adduser [id exist]");
$result = $myModel->addUser(new User(3, 'olga155', 'olga155', 'staff', 'olga155@gmail.com'));
print_r($result);
print_r(PHP_EOL);
echo "<br>";
echo "<br>";



print_r("TEST: update [id exist]".PHP_EOL);
echo "<br>";
$result = $myModel->updateUser(new User(3, 'NewUsername', 'NewPassword', 'staff', 'newemail@example.com'));
print_r($result);
echo "<br>";

print_r("TEST: update [id not exist]".PHP_EOL);
echo "<br>";
$result = $myModel->updateUser(new User(24, 'NewUsername', 'NewPassword', 'staff', 'newemail@example.com'));
print_r($result);
echo "<br>";


print_r("TEST: userByroll [roll exist]".PHP_EOL);
echo "<br>";
$users = $myModel->searchByUserRoll('staff');
foreach($users as $user){
    echo $user;
    echo "<br>";
}
print_r("TEST: userByroll [roll not exist]".PHP_EOL);
echo "<br>";
$users = $myModel->searchByUserRoll('madmin');
foreach($users as $user){
    echo $user;
    echo "<br>";
}

print_r("TEST: searchByCredentials [user exist]" . PHP_EOL);
echo "<br>";
$result = $myModel->searchUserByCredentials('ket15', 'pass15');
var_dump($result);
echo "<br>";

print_r("TEST: searchByCredentials [user not exist]" . PHP_EOL);
echo "<br>";
$result = $myModel->searchUserByCredentials('ket7', 'pass14');
var_dump($result);
echo "<br>";

print_r ("*****************PRODUCTS*****************".PHP_EOL);
echo "<br>";
print_r("TEST: search all [find]".PHP_EOL);
echo "<br>";
$products = $myModel->searchAllProducts();
foreach($products as $product){
    echo $product;
    echo "<br>";
}

echo "<br>";
print_r("TEST: search by id [find]".PHP_EOL);
echo "<br>";
$product = $myModel->searchProductById(1);
echo $product;
echo "<br>";

echo "<br>";
print_r("TEST: search by id [not find]".PHP_EOL);
echo "<br>";
$products = $myModel->searchProductById(4);
echo "The user doesn't exist.";
echo "<br>";



echo "<br>";
print_r("TEST: search by price [find]".PHP_EOL);
echo "<br>";
$products = $myModel->searchProductByPrice(15);
foreach($products as $product){
    echo $product;
    echo "<br>";
}

echo "<br>";
print_r("TEST: search by price [not find]".PHP_EOL);
echo "<br>";
$products = $myModel->searchProductByPrice(5);
echo "The producto has not been found.";
echo "<br>";


echo "<br>";
print_r("TEST: search by size [find]".PHP_EOL);
echo "<br>";
$products = $myModel->searchProductBySize('S');
foreach($products as $product){
    echo $product;
    echo "<br>";
}

echo "<br>";
print_r("TEST: search by size [not find]".PHP_EOL);
echo "<br>";
$products = $myModel->searchProductBySize('XL');
echo "The producto has not been found.";
echo "<br>";

echo "<br>";
print_r("TEST: search by size [size not allow]".PHP_EOL);
echo "<br>";
$products = $myModel->searchProductBySize('XXL');
echo "Size not allow.";
echo "<br>";

print_r("TEST: add product [id not exist]");
$result = $myModel->addProduct(new Product(4, 'jeans', 'L', 25.95, 5));
print_r($result);
print_r(PHP_EOL);
echo "<br>";

print_r("TEST: add product [id exist]");
$result = $myModel->addProduct(new Product(3, 'dark jeans', 'L', 25.95, 5));
print_r($result);
print_r(PHP_EOL);
echo "<br>";
echo "<br>";

print_r("TEST: add product [id not exist, size not allow]");
echo "<br>";
$result = $myModel->addProduct(new Product(4, 'blue jeans', 'XXL', 25.95, 5));
print_r($result);
print_r(PHP_EOL);
echo "<br>";



print_r(PHP_EOL);
echo "<br>";
print_r("TEST: delete product [null]".PHP_EOL);
$result = $myModel->deleteProduct(new Product());
print_r($result);
print_r(PHP_EOL);
echo "<br>";
print_r("TEST: delete product [not exist]".PHP_EOL);
$result = $myModel->deleteProduct(new Product(24));
print_r($result);
print_r(PHP_EOL);
echo "<br>";
print_r("TEST: delete product [exist]".PHP_EOL);
$result = $myModel->deleteProduct(new Product(4));
print_r($result);
print_r(PHP_EOL);
echo "<br>";



print_r("TEST: update product[id exist]".PHP_EOL);
echo "<br>";
$result = $myModel->updateProduct(new Product(3,'dark jeans','L',25.95,5));
print_r($result);
echo "<br>";

print_r("TEST: update product [id not exist]".PHP_EOL);
echo "<br>";
$result = $myModel->updateProduct(new Product(23,'jeans','L',25.95,5));
print_r($result);
echo "<br>";

print_r("TEST: update product[size not allow exist]".PHP_EOL);
echo "<br>";
$result = $myModel->updateProduct(new Product(3,'jeans','XXL',25.95,5));
print_r($result);
echo "<br>";



