<script type="text/javascript">
  function submitForm(event) {
      var target = event.target;
      var buttonId = target.id;
      var myForm = document.getElementById('user-login');
      myForm.action.value = button.id;
      myForm.submit();
      return false;
  }
</script>

<?php
$user = $params['user'] ?? null;
$action = $params['action'] ?? "login";
$result = $params['result'] ?? null;
$username = $params['username'] ?? null;
$isSessionActive = isset($_SESSION['username']); // Check the session

if(!$isSessionActive) {
  if (is_null($user)) {
    $user = new User(0, "");
  }
  if (!is_null($result)) {
    echo <<<EOT
        <div><p class="alert">$result</p></div>
  EOT;
  }
  
  echo <<<EOT
    
        <h2>Login form</h2>
        <form id="user-login" method="post" action="index.php">
            <fieldset>
                <label for="username">Username:</label>
                <input type="username" class="form-control" id="username" placeholder="Enter username" name="username" value="$username">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
            </fieldset>
            <fieldset>
  
              <button type="submit" id="user/login" name="user/login" onclick="return submitForm(event);">Submit</button>
              <input name="action" id="action" hidden="hidden" value="user/login"/>    
            </fieldset>
        </form>
   
  EOT;

} else {
  if (is_null($user)) {
    $user = new User(0, "");
  }
  if (!is_null($result)) {
    echo <<<EOT
        <div><p class="alert">$result</p></div>
  EOT;
  }
  
  echo <<<EOT
    
        <h2>Login form</h2>
        <form id="user-login" method="post" action="index.php">
            <fieldset>
                <label for="username">Username:</label>
                <input type="username" class="form-control" id="username" placeholder="Enter username" name="username" value="$username">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
            </fieldset>
            <fieldset>
  
              <button type="submit" id="user/login" name="user/login" onclick="return submitForm(event);" disabled>Submit</button>
              <input name="action" id="action" hidden="hidden" value="user/login"/>    
            </fieldset>
        </form>
   
  EOT;
   

}
