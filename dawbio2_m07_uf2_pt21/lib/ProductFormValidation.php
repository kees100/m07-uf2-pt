<?php
require_once 'model/product.php';

/**
 * Description of ItemFormValidation
 * Provides validation to get data from item form.
 * @author ProvenSoft
 */
class ProductFormValidation {
    
    /**
     * validates and gets data from item form.
     * @return Product the item with the given data or null if data is not present and valid.
     */
    public static function getData() {
        $productObj = null;
        $id = 0;
        //retrieve id sent by client.
        if (filter_has_var(INPUT_GET, 'id')) {
            $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT); 
        }
        $description = "";
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'description')) {
            $description = filter_input(INPUT_GET, 'description'); 
        }
        $size = "";
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'size')) {
            $size = filter_input(INPUT_GET, 'size'); 
        }
        $price = 0;
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'price')) {
            $price = filter_input(INPUT_GET, 'price'); 
        }
        $stock = 0;
        //retrieve item sent by client.
        if (filter_has_var(INPUT_GET, 'stock')) {
            $stock = filter_input(INPUT_GET, 'stock'); 
        }
        //if (!empty($id) && !empty($title) && !empty($content), etc) { 
            //they exists and they are not empty
            $itemObj = new Product($id, $description, $size, $price, $stock);
        //}
        return $itemObj;
    }
    
}
