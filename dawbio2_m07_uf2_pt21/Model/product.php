<?php   
/**
 * Product definition class.
 * @author Ketsia
 */
class Product {
    private ?int $id;
    private ?string $description;
    private ?string $size;
    private ?float $price;
    private ?int $stock;

    /**
     * Product constructor.
     * @param int|null $id The product ID.
     * @param string|null $description The product description.
     * @param string|null $size The product size.
     * @param float|null $price The product price.
     * @param int|null $stock The product stock quantity.
     */
    public function __construct(int $id = null, string $description = null, string $size = null, float $price = null, int $stock = null) {
        $this->id = $id;
        $this->description = $description;
        $this->size = $size;
        $this->price = $price;
        $this->stock = $stock;
    }

    /**
     * Get the product ID.
     * @return int|null The product ID.
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * Set the product ID.
     * @param int $id The product ID.
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * Get the product description.
     * @return string|null The product description.
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Set the product description.
     * @param string $description The product description.
     */
    public function setDescription(string $description) {
        $this->description = $description;
    }

    /**
     * Get the product size.
     * @return string|null The product size.
     */
    public function getSize(): ?string {
        return $this->size;
    }

    /**
     * Set the product size, only if it is a valid size.
     * @param string $size The product size.
     */
    public function setSize(string $size) {
        if ($this->isValidSize($size)) {
            $this->size = $size;
        }    
    }

    /**
     * Get the product price.
     * @return float|null The product price.
     */
    public function getPrice(): ?float {
        return $this->price;
    }

    /**
     * Set the product price.
     * @param float $price The product price.
     */
    public function setPrice(float $price) {
        $this->price = $price;
    }

    /**
     * Get the product stock quantity.
     * @return int|null The product stock quantity.
     */
    public function getStock(): ?int {
        return $this->stock;
    }

    /**
     * Set the product stock quantity.
     * @param int $stock The product stock quantity.
     */
    public function setStock(int $stock) {
        $this->stock = $stock;
    }

    /**
     * Function to check if a Product object is empty.
     * @param Product $product The Product object to check.
     * @return bool True if the Product object is empty, false otherwise.
     */
    private function isProductEmpty(Product $product): bool {
        return is_null($product->getId())
            && is_null($product->getDescription())
            && is_null($product->getSize())
            && is_null($product->getPrice())
            && is_null($product->getStock());
    }

    /**
     * Check if a given size is valid for a product.
     * @param string $size The size to check.
     * @return bool True if the size is valid, false otherwise.
     */
    public function isValidSize(string $size): bool {
        $allowedSizes = ['M', 'S', 'L', 'XL'];
        return in_array($size, $allowedSizes);
    }

    /**
     * Convert the Product object to a string representation.
     * @return string The string representation of the Product object.
     */
    public function __toString() {
        return "Item{[id=$this->id][description=$this->description][size=$this->size][price=$this->price][stock=$this->stock]}";
    } 
}
