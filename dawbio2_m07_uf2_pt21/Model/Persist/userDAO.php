<?php 

/**
 * User persistence class.
 * @author Ketsia
 */

class UserFileDao {
    const FILEPATH = 'Files/users.txt';
    const DELIMITER = ';';
    
    public function __construct() {
      
    }

    /**
     * Select all users from the data source.
     * @return array List of users or an empty array if there are no users or
     * if the file can't be accessed.
     */
    public function selectAll(): array {
        $data = array();
        if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
            $handle = \fopen(self::FILEPATH, 'r');  // returns false on error.
            if ($handle) {
                while (!\feof($handle)) {
                    $line = \fgets($handle);
                    if ($line) {
                        list($id, $username, $password, $role, $email) = \explode(self::DELIMITER, $line);
                        $id = (int)$id;

                        $user = new User($id, $username, $password, $role, $email);
                        array_push($data, $user);
                    }
                }
                \fclose($handle);            
            }
        }
        return $data;
    }

    /**
     * Function for retrieving a user by their ID.
     * @param User $SearchUser The user object to search for.
     * @return User|null The User object if found, or null if not found.
     */
    public function searchById(User $SearchUser): ?User {
        $userFound = null;
        $userList = $this->selectAll();
        foreach ($userList as $user) {
            if ($user->getId() === $SearchUser->getId()) {
                $userFound = $user;
                return $userFound; // User found
            }
        }
    
        return $userFound; // User not found
    }

    /**
     * Add a user to the data source.
     * @param User $user The user object to be added.
     * @return int Returns 1 if the user was successfully added, 0 otherwise.
     */
    public function addUser(User $user): int {
        $result = 0;
        $userList = $this->selectAll();
        $existingUserIndex = $this->arraySearchIndex($userList, $user);
    
        if ($existingUserIndex === -1) {
            $new_user = $this->fromObjectToFields($user);
    
            if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
                $handle = \fopen(self::FILEPATH, 'a+');  
                if ($handle) {
                    if (\filesize(self::FILEPATH) > 0) {
                        fwrite($handle, PHP_EOL);
                    }
                    $new_line = implode(self::DELIMITER, $new_user);
                    $success = fwrite($handle, $new_line); 
    
                    if ($success == true) {
                        $result = 1;
                    }
    
                    fclose($handle);
                } else {
                    $result = 0;
                }
            } else {
                $result = 0;
            }
        } 
    
        return $result;
    }

    /**
     * Delete a user from the data source if it exists.
     * @param User $user The user object to be deleted.
     * @return int Number of users deleted (1 if successful, 0 otherwise).
     */
    public function delete(User $user): int {
        $result = 0;
        $userList = $this->selectAll();
        $index = $this->arraySearchIndex($userList, $user);

        if ($index >= 0) {  // if found.
            array_splice($userList, $index, 1);  // remove an object.
            $result = 1;
            $this->saveAll($userList);  // save list to file.
        }

        return $result;
    }

    /**
     * Update the information of a user in the data source.
     * @param User $user The user object containing updated information.
     * @return int Returns 1 if the user was successfully updated, 0 otherwise.
     */
    public function update(User $user): int {
        $result = 0; // Initialize the result variable to 0 (indicating no update)
        $userList = $this->selectAll();
        $index = $this->arraySearchIndex($userList, $user);

        if ($index !== -1) {
            $userList[$index]->setUsername($user->getUsername());
            $userList[$index]->setPassword($user->getPassword());
            $userList[$index]->setRole($user->getRole());
            $userList[$index]->setEmail($user->getEmail());

            $this->saveAll($userList);
            $result = 1; 
        }

        return $result;
    }

    /**
     * Search for users by role.
     * @param string $roll The role to search for.
     * @return array|null Array of users with the specified role or null if not found.
     */
    public function searchByRoll(string $roll): ?array {
        $usersByRoll = array();
        if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
            $handle = \fopen(self::FILEPATH, 'r');  
            if ($handle) {
                while (!\feof($handle)) {
                    $line = \fgets($handle);
                    if ($line) {
                        list($id, $username, $password, $role, $email) = \explode(self::DELIMITER, $line);
                        $id = (int)$id;
    
                        $user = new User($id, $username, $password, $role, $email);
    
                        if ($user->getRole() === $roll) {
                            array_push($usersByRoll, $user);
                        }
                    }
                }
                \fclose($handle);
            } else {
                return null; // No users found
            }
        }
        return $usersByRoll;
    }

    /**
     * Search for a user by credentials (username and password).
     * @param string $inputUsername The input username to search for.
     * @param string $inputPassword The input password to search for.
     * @return User|null The User object if found, or null if not found.
     */
    public function searchByCredentials(string $inputUsername, string $inputPassword): ?User {
        $foundUser = null;
        if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
            $handle = \fopen(self::FILEPATH, 'r');  
            if ($handle) {
                while (!\feof($handle)) {
                    $line = \fgets($handle);
                    if ($line) {
                        list($id, $username, $password, $role, $email) = \explode(self::DELIMITER, $line);
                        $id = (int)$id;
    
                        $validUser = new User($id, $username, $password, $role, $email);
    
                        if ($validUser->getUsername() === $inputUsername && $validUser->getPassword() === $inputPassword) {
                            $foundUser = $validUser;;
                        }
                    }
                }
                \fclose($handle);
            } else {
                $foundUser = null; // User not found
            }
        }
        return $foundUser;
    }
           

    /**
     * searches object in array.
     * @param array $list the array to search in.
     * @param User $obj the user to search.
     * @return int position of the element searched or -1 if not found.
     */
    private function arraySearchIndex(array $list, User $obj): int {
        $index = -1;
        for ($i=0; $i<count($list); $i++) {
            if ($list[$i]->getId() == $obj->getId()) {
                $index = $i;
                break;
            }
        }
        return $index;
    }

     /**
     * saves array with all data in file.
     * @param $data the array to save to file.
     * @param bool $appendNewLine Si se debe añadir un salto de línea al final del archivo.
     * @return int number of elements written.
     */
 
    public function saveAll(array $data): int {
        $handle = \fopen(self::FILEPATH, "wb");  //open file to read.
        $count = 0;
        foreach ($data as $obj) {
            //convert object to csv.
            $fields = $this->fromObjectToFields($obj);
            $success = fwrite($handle, implode(self::DELIMITER, $fields)); 
            $count += ($success) ? 1 : 0;
        }
        fclose($handle);
        return $count;
    }



    /**
     * Convert an user object to an array of fields
     * @param User to be converted
     * @return array with object properties as fields
     */
    private function fromObjectToFields (User $obj):array{
        $fields = array();
        $fields[0] = $obj->getId();
        $fields[1] = $obj->getUsername();
        $fields[2] = $obj->getPassword();
        $fields[3] = $obj->getRole();
        $fields[4] = $obj->getEmail();
        return $fields;
    }
}




