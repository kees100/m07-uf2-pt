<table>
    <tr>
        <th>Id</th>
        <th>Description</th>
        <th>Size</th>
        <th>Stock</th>
    </tr>
    <?php
    //display list of products in a table.
    $productList = $params['productList'];
    // $params contains variables passed in from the controller.
    foreach ($productList  as $product) {
        echo <<<EOT
        <tr>
            <td>{$product->getId()}</td>
            <td><a href="index.php?id={$product->getId()}&action=findProduct">{$product->getDescription()}</a></td>
            <td>{$product->getSize()}</td>
            <td>{$product->getStock()}</td>
        </tr>               
EOT;
    }
    ?>
</table>

