<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


if (isset($_SESSION['username']) && isset($_SESSION['role'])) {
   
    $username = $_SESSION['username'];
    $role = $_SESSION['role'];
    echo <<<EOT
        <nav class="navbar navbar-default">
        <div class="container col-md-10">
            <div class="navbar-header">
                <a class="navbar-brand" href="https://www.proven.cat">Proven Store</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.php?action=home">Home</a></li>
                    <li><a href="index.php?action=logout">Logout</a></li>
                    <li><a href="index.php?action=users/listAll">List all users</a></li>
                    <li><a href="index.php?action=userForm">Users form</a></li>
                    <li><a href="index.php?action=products/listAll">List all products</a></li>
                    <li><a href="index.php?action=productForm">Search products</a></li>
                </ul>
            </div>
        </div>
    </nav>
    EOT;
 
} else {
   
    echo <<<EOT

    <nav class="navbar navbar-default">
        <div class="container col-md-10">
            <div class="navbar-header">
                <a class="navbar-brand" href="https://www.proven.cat">Proven Store</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.php?action=home">Home</a></li>
                    <li><a href="index.php?action=login">Login</a></li>
                    <li><a href="index.php?action=products/listAll">List all products</a></li>
                    <li><a href="index.php?action=productForm">Search products</a></li>
                </ul>
            </div>
        </div>
    </nav>
    EOT;

} 


 