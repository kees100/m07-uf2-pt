<?php 

/**
 * Product persistence class.
 * @author Ketsia and Proven
 */

class ProductFileDao {
    const FILEPATH = 'Files/products.txt';
    const DELIMITER = ';';
    
    public function __construct() {
      
    }
    
    /**
     * Select all products from the data source.
     * @return array list of products or an empty array if there are no products or
     * if the file can't be accessed.
     */
    public function selectAll(): array {
        $data = array();
        if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
            $handle = \fopen(self::FILEPATH, 'r');  // returns false on error.
            if ($handle) {
                while (!\feof($handle)) {
                    $line = \fgets($handle);
                    if ($line) {
                        list($id, $description, $size, $price, $stock) = \explode(self::DELIMITER, $line);
                        $id = (int)$id;

                        $product = new Product($id, $description, $size, $price, $stock);
                        array_push($data, $product);
                    }
                }
                \fclose($handle);            
            }
        }
        return $data;
    }

    /**
     * Add a product to the data source.
     * @param Product $product The product object to be added.
     * @return int Returns 1 if the product was successfully added, 0 otherwise.
     */
    public function addProduct(Product $product): int {
        $result = 0;
        $productList = $this->selectAll();
        $index = $this->arraySearchIndex($productList, $product);

        if (!$product->isValidSize($product->getSize())) {
            return 0;
        }
    
        if ($index === -1) {
            $new_product = $this->fromObjectToFields($product);
    
            if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
                $handle = \fopen(self::FILEPATH, 'a');  
                if ($handle) {
                    $new_line = implode(self::DELIMITER, $new_product);
                    $success = fwrite($handle, $new_line); 
    
                    if ($success == true) {
                        $result = 1;
                    }
    
                    fclose($handle);
                } else {
                    $result = 0;
                }
            } else {
                $result = 0;
            }
        } 
    
        return $result;
    }

    /**
     * Delete a product from the data source if it exists.
     * @param Product $product The product object to be deleted.
     * @return int Number of products deleted (1 if successful, 0 otherwise).
     */
    public function delete(Product $product): int {
        $result = 0;
        $productList = $this->selectAll();
        $index = $this->arraySearchIndex($productList, $product);

        if ($index >= 0) {  // if found.
            array_splice($productList, $index, 1);  // remove an object.
            $result = 1;
            $this->saveAll($productList);  // save list to file.
        }
        return $result;
    }

    /**
     * Update the information of a product in the data source.
     * @param Product $product The product object containing updated information.
     * @return int Returns 1 if the product was successfully updated, 0 otherwise.
     */
    public function update(Product $product): int {
        $result = 0; // Initialize the result variable to 0 (indicating no update)
        $productList = $this->selectAll();
        $index = $this->arraySearchIndex($productList, $product);

        if ($index !== -1) {
            $productList[$index]->setDescription($product->getDescription());
            $productList[$index]->setSize($product->getSize());
            $productList[$index]->setPrice($product->getPrice());
            $productList[$index]->setStock($product->getStock());
       
            $this->saveAll($productList);
            $result = 1; 
        }

        return $result;
    }
    

    /**
     * Search for products by size.
     * @param string $searchSize The size to search for.
     * @return array|null Array of products with the specified size or null if not found.
     */
    public function searchBySize(string $searchSize): ?array {
        $productsBySize = array();

        if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
            $handle = \fopen(self::FILEPATH, 'r');  
            if ($handle) {
                while (!\feof($handle)) {
                    $line = \fgets($handle);
                    if ($line) {
                        list($id, $description, $size, $price, $stock) = \explode(self::DELIMITER, $line);
                        $id = (int)$id;
    
                        $product = new Product ($id, $description, $size, $price, $stock);
    
                        if ($product->getSize() === $searchSize) {
                            array_push($productsBySize, $product);
                        }
                    }
                }
                \fclose($handle);
            } else {
                return null; // No products found
            }
        }
        return $productsBySize;
    }

    /**
     * Search for products by price.
     * @param float $maxPrice The maximum price to search for.
     * @return array|null Array of products with a price less than $maxPrice or null if not found.
     */
    public function searchByPrice(float $maxPrice): ?array {
        $productsByPrice = [];
    
        if (\file_exists(self::FILEPATH) && \is_readable(self::FILEPATH)) {
            $handle = \fopen(self::FILEPATH, 'r');  
    
            if ($handle) {
                while (!\feof($handle)) {
                    $line = \fgets($handle);
    
                    if ($line) {
                        list($id, $description, $size, $price, $stock) = \explode(self::DELIMITER, $line);
                        $id = (int)$id;
                        $price = (float)$price;
    
                        $product = new Product($id, $description, $size, $price, $stock);
    
                        if ($product->getPrice() < $maxPrice) {
                            array_push($productsByPrice, $product);
                        }
                    }
                }
    
                \fclose($handle);
                
                if (empty($productsByPrice)) {
                    return null; // No products found
                }
            }
        }
    
        return $productsByPrice;
    }

    /**
     * Search for a product by its ID.
     * @param Product $SearchProduct The product object to search for.
     * @return Product|null The product object if found, or null if not found.
     */
    public function searchById(Product $SearchProduct): ?Product {
        $productFound = null;
        $productList = $this->selectAll();

        foreach ($productList as $product) {
            if ($product->getId() === $SearchProduct->getId()) {
                $productFound= $product;
                return $productFound; // Product found
            }
        }
    
        return $productFound; //product not found
    }

    /**
     * searches object in array.
     * @param array $list the array to search in.
     * @param Product $obj the user to search.
     * @return int position of the element searched or -1 if not found.
     */
    private function arraySearchIndex(array $list, Product $obj): int {
        $index = -1;
        for ($i=0; $i<count($list); $i++) {
            if ($list[$i]->getId() == $obj->getId()) {
                $index = $i;
                break;
            }
        }
        return $index;
    }

     /**
     * saves array with all data in file.
     * @param $data the array to save to file.
     * @return int number of elements written.
     */
    public function saveAll(array $data): int {
        $handle = \fopen(self::FILEPATH, "wb");  //open file to read.
        $count = 0;
        foreach ($data as $obj) {
            //convert object to csv.
            $fields = $this->fromObjectToFields($obj);
            $success = fwrite($handle, implode(self::DELIMITER, $fields) . PHP_EOL); 
            $count += ($success) ? 1 : 0;
        }
        fclose($handle);
        return $count;
    }

    /**
     * Convert an product object to an array of fields
     * @param Product to be converted
     * @return array with object properties as fields
     */
    private function fromObjectToFields (Product $obj):array{
        $fields = array();
        $fields[0] = $obj->getId();
        $fields[1] = $obj->getDescription();
        $fields[2] = $obj->getSize();
        $fields[3] = $obj->getPrice();
        $fields[4] = $obj->getStock();
        return $fields;
    }
}




