<?php
    session_start();
    //session is used here to simulate data persistance (only needed for array persistance).
    //require_once 'model/persist/ItemArrayDao.php';
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);   
    ini_set('error_reporting', E_ALL);

    // Verifica si hay una sesión iniciada y si hay datos de usuario almacenados en ella
    if(isset($_SESSION['username']) && isset($_SESSION['role'])) {
      // Obtén los datos del usuario desde la sesión
      $username = $_SESSION['username'];
      $role = $_SESSION['role'];
      
    } 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Proven store</title>
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/styless.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <?php
        include "views/top-menu.php";
      ?>
      <div class="container">
      <h2>Proven store</h2>
      <?php
        //dynamic html content generated here by controller.
        require_once 'Controllers/controller.php';
        (new Controller())->processRequest();
      ?>
      </div>
      <?php include_once "views/footer.php";?>
    </div>
  </body>
</html>

