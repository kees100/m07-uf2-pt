<?php
require_once 'user.php';
require_once 'product.php';

require_once 'Persist/productDAO.php';
require_once 'Persist/userDAO.php';

/**
 * Service class to provide data.
 * @author Ketsia
 */
class Model {
    private $UserdataSource;
    private $ProductdataSource;
 
    public function __construct() {
        // Initialize data sources for users and products
        $this->UserdataSource = new UserFileDao();
        $this->ProductdataSource = new ProductFileDao();
    }

    /**
    * Function for updating user information.
    * @param User $newUserData The User object containing updated information.
    * @return int Returns 1 if the user was successfully updated, 0 otherwise.
    */
    public function updateUser(User $newUserData): int {
        $result = 0;
        if (!is_null($newUserData) && !is_null($newUserData->getId())) {
            $result = $this->UserdataSource->update($newUserData);
        }
        return $result;
    }

    /**
     * Delete a user if it exists.
     * @param User $user The User object to be deleted.
     * @return int Number of users deleted.
     */
    public function deleteUser(User $user): int {
        $result = 0;
        if (!is_null($user) && !is_null($user->getId())) {
            $result = $this->UserdataSource->delete($user);
        }
        return $result;
    }

    /**
     * Retrieve all users from the data source.
     * @return User[] An array of User objects.
     */
    public function searchAllUsers(): array {
        return $this->UserdataSource->selectAll();
    }

    /**
     * Retrieve a user by their ID.
     * @param int $id The ID of the user to retrieve.
     * @return User|null The User object if found, or null if not found.
     */
    public function searchUserById(int $id): ?User {
        return $this->UserdataSource->searchById(new User($id, null, null, null, null));
    }

    /**
     * Add a new user to the data source.
     * @param User $user The User object to be added.
     * @return int Returns 1 if the user was successfully added, 0 otherwise.
     */
    public function addUser(User $user): int {
        return $this->UserdataSource->addUser($user);
    }

    /**
     * Retrieve users by a specific user role.
     * @param string $roll The user role to filter users.
     * @return User[] An array of User objects matching the specified role.
     */
    public function searchByUserRoll(string $roll): array {
        return $this->UserdataSource->searchByRoll($roll);
    }

    /**
     * Retrieve a user by credentials (username and password).
     * @param string $username The username of the user.
     * @param string $password The password of the user.
     * @return User|null The User object if credentials are valid, or null otherwise.
     */
    public function searchUserByCredentials(string $username, string $password): ?User {
        return $this->UserdataSource->searchByCredentials($username, $password);
    }

    /**
     * Retrieve all products from the data source.
     * @return Product[] An array of Product objects.
     */
    public function searchAllProducts(): array {
        return $this->ProductdataSource->selectAll();
    }

    /**
     * Retrieve a product by its ID.
     * @param int $id The ID of the product to retrieve.
     * @return Product|null The Product object if found, or null if not found.
     */
    public function searchProductById(int $id): ?Product {
        return $this->ProductdataSource->searchById(new Product($id, null, null, null, null));
    }

    /**
     * Add a new product to the data source.
     * @param Product $product The Product object to be added.
     * @return int Returns 1 if the product was successfully added, 0 otherwise.
     */
    public function addProduct(Product $product): int {
        return $this->ProductdataSource->addProduct($product);
    }

    /**
     * Update product information in the data source.
     * @param Product $new_product The Product object containing updated information.
     * @return int Returns 1 if the product was successfully updated, 0 otherwise.
     */
    public function updateProduct(Product $new_product): int {
        $result = 0;
        if (!is_null($new_product) && !is_null($new_product->getId())) {
            $result = $this->ProductdataSource->update($new_product);
        }
        return $result;
    }

    /**
     * Delete a product if it exists.
     * @param Product $product The Product object to be deleted.
     * @return int Number of products deleted.
     */
    public function deleteProduct(Product $product): int {
        $result = 0;
        if (!is_null($product) && !is_null($product->getId())) {
            $result = $this->ProductdataSource->delete($product);
        }
        return $result;
    }

    /**
     * Retrieve products by size.
     * @param string $size The size to filter products.
     * @return Product[] An array of Product objects matching the specified size.
     */
    public function searchProductBySize(string $size): ?array {
        return $this->ProductdataSource->searchBySize($size);
    }

    /**
     * Retrieve products by price.
     * @param float $price The price to filter products.
     * @return Product[] An array of Product objects matching the specified price.
     */
    public function searchProductByPrice(float $price): ?array {
        return $this->ProductdataSource->searchByPrice($price);
    }
}
