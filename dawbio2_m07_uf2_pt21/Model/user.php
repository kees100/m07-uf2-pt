<?php   
/**
 * User class definition.
 * @author Ketsia
 */
class User {
    private ?int $id;
    private ?string $username;
    private ?string $password;
    private ?string $role;
    private ?string $email;
    
    /**
     * User constructor.
     * @param int|null $id The user ID.
     * @param string|null $username The username.
     * @param string|null $password The user password.
     * @param string|null $role The user role.
     * @param string|null $email The user email.
     */
    public function __construct(int $id = null, string $username = null, string $password = null, string $role = null, string $email = null) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->role = $role;
        $this->email = $email;
    }
 
    /**
     * Get the user ID.
     * @return int|null The user ID.
     */
    public function getId(): ?int {
        return $this->id;
    }
    
    /**
     * Set the user ID.
     * @param int $id The user ID.
     */
    public function setId(int $id) {
        $this->id = $id;
    }
    
    /**
     * Get the username.
     * @return string|null The username.
     */    
    public function getUsername(): ?string {
        return $this->username;
    }    
    
    /**
     * Set the username.
     * @param string $username The username.
     */
    public function setUsername(string $username) {
        $this->username = $username;
    }
    
    /**
     * Get the user password.
     * @return string|null The user password.
     */
    public function getPassword(): ?string {
        return $this->password;
    }

    /**
     * Set the user password.
     * @param string $password The user password.
     */
    public function setPassword(string $password) {
        $this->password = $password;
    }

    /**
     * Get the user role.
     * @return string|null The user role.
     */
    public function getRole(): ?string {
        return $this->role;
    }

    /**
     * Set the user role.
     * @param string $role The user role.
     */
    public function setRole(string $role) {
        $this->role = $role;
    }

    /**
     * Get the user email.
     * @return string|null The user email.
     */
    public function getEmail(): ?string {
        return $this->email;
    }

    /**
     * Set the user email.
     * @param string $email The user email.
     */
    public function setEmail(string $email) {
        $this->email = $email;
    }

    /**
     * Convert the User object to a string representation.
     * @return string The string representation of the User object.
     */
    public function __toString() {
        return "Item{[id=$this->id][username=$this->username][password=$this->password][role=$this->role][email=$this->email]}";
    }
}
