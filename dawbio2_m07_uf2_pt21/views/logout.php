</script>
<?php
$user = $params['user'] ?? null;
$action = $params['action'] ?? "logout";
$result = $params['result'] ?? null;
$username = $params['username'] ?? null;
$isSessionActive = isset($_SESSION['userValid']);

if ($isSessionActive) {
    if (is_null($user)) {
        $user = new User(0, "");
      }
      if (!is_null($result)) {
        echo <<<EOT
            <div><p class="alert">$result</p></div>
      EOT;
      }
      
      echo <<<EOT
        <h2>Confirm Logout</h2>
        <p>Are you sure you want to logout?</p>
        <form method="post" action="index.php" id="user-logout">
            <button type="submit" name="user/logout" id="user/logout" onclick="return submitForm(event);">Logout</button>
            <input name="action" id="action" hidden="hidden" value="user/logout"/>   
        </form>
    EOT;

} else {
    if (is_null($user)) {
        $user = new User(0, "");
      }
      if (!is_null($result)) {
        echo <<<EOT
            <div><p class="alert">$result</p></div>
      EOT;
      }
      
      echo <<<EOT
      <p>You are not logged in.</p>
      <p>Log in <a href="index.php?action=login">here</a>.</p>
    EOT;
    
} 